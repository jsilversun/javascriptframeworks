'use strict';

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

class Map extends Model {
  static get createdAtColumn() {
    return 'createdAt';
  }

  static get updatedAtColumn() {
    return null;
  }

  static get table() {
    return 'Map';
  }

  columns() {
    return this.hasMany('App/Models/Column', 'id', 'Map_id');
  }
}

module.exports = Map;
