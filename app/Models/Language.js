'use strict';

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

class Language extends Model {
  static get primaryKey() {
    return 'code';
  }

  static get createdAtColumn() {
    return 'createdAt';
  }

  static get updatedAtColumn() {
    return null;
  }

  static get table() {
    return 'Language';
  }

  countries () {
    return this.hasMany('App/Models/Country', 'code', 'Language_code');
  }
}

module.exports = Language;
