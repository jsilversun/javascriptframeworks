'use strict';

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

class Category extends Model {
  static get createdAtColumn() {
    return 'createdAt';
  }

  static get updatedAtColumn() {
    return null;
  }

  static get table() {
    return 'Category';
  }

  translations() {
    return this
      .belongsToMany('App/Models/Language', 'Category_id', 'Language_code')
      .pivotTable('CryLge')
      .withPivot(['name', 'description']);
  }
}

module.exports = Category;
