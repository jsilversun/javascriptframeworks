'use strict';

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

class Board extends Model {
  static get createdAtColumn() {
    return 'createdAt';
  }

  static get updatedAtColumn() {
    return null;
  }

  static get table() {
    return 'Board';
  }

  columns() {
    return this
      .belongsToMany('App/Models/Column', 'Board_id', 'Column_id')
      .pivotTable('BrdCmn')
      .withPivot(['customName', 'isMeasurement','isDisplayable', 'isLocation', 'isLegend']);
  }

  categories() {
    return this
      .belongsToMany('App/Models/Category', 'Board_id', 'Category_id')
      .pivotTable('BrdCry')
      .withPivot(['position']);
  }

  translations() {
    return this
      .belongsToMany('App/Models/Language', 'Board_id', 'Language_code')
      .pivotTable('BrdLge')
      .withPivot(['name', 'description']);
  }
}

module.exports = Board;
