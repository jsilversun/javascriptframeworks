'use strict';

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

class Column extends Model {
  static get createdAtColumn() {
    return 'createdAt';
  }

  static get updatedAtColumn() {
    return null;
  }

  static get table() {
    return 'Column';
  }
}

module.exports = Column;
