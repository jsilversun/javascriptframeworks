'use strict';

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with maps
 */
let Map = use('App/Models/Map');
let index = 'tableId';

class MapController {
  /**
   * Show a list of all maps.
   * GET maps
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({request, response, view}) {
    return await Map.query().with('columns').fetch();
  }

  /**
   * Render a form to be used for creating a new map.
   * GET maps/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({request, response, view}) {
  }

  /**
   * Create/save a new map.
   * POST maps
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({request, response}) {
    let status = 200;
    let message = 'Successful insertion';
    response.statusCode = 404;
    try {
      await Map.create({...request.post()});
    } catch (e) {
      status = 500;
      ({message} = e);
    }
    return response.status(status).json(message);
  }

  /**
   * Display a single map.
   * GET maps/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({params, request, response, view}) {
    return await Map.findByOrFail(index, params.id);
  }

  /**
   * Render a form to update an existing map.
   * GET maps/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({params, request, response, view}) {
  }

  /**
   * Update map details.
   * PUT or PATCH maps/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({params, request, response}) {
    let status = 200;
    let message = 'Successful update';
    let instance = {};
    response.statusCode = 404;
    try {
      instance = await Map.findByOrFail(index, params.id);
      instance.merge({...request.post()});
      await instance.save();
    } catch (e) {
      status = 500;
      ({message} = e);
    }
    return response.status(status).json(message);
  }

  /**
   * Delete a map with id.
   * DELETE maps/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({params, request, response}) {
    let status = 200;
    let message = 'Successful delete';
    response.statusCode = 404;
    try {
      const instance = await Map.findByOrFail(index, params.id);
      await instance.delete();
    } catch (e) {
      status = 500;
      ({message} = e);
    }
    return response.status(status).json(message);
  }
}

module.exports = MapController;
