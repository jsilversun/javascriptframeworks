'use strict';
const Board = use('App/Models/Board');
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with boards
 */
let index = 'icon';

class BoardController {
  /**
   * Show a list of all boards.
   * GET boards
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({request, response, view}) {
    return Board
      .query()
      .with('categories')
      .with('translations')
      .with('columns')
      .fetch();
  }

  /**
   * Render a form to be used for creating a new board.
   * GET boards/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({request, response, view}) {
  }

  /**
   * Create/save a new board.
   * POST boards
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({request, response}) {
    let status = 200;
    let message = 'Successful insertion';
    response.statusCode = 404;
    try {
      await Board.create({...request.post()});
    } catch (e) {
      status = 500;
      ({message} = e);
    }
    return response.status(status).json(message);
  }

  /**
   * Display a single board.
   * GET boards/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({params, request, response, view}) {
    return Board.findByOrFail(index, params.id);
  }

  /**
   * Render a form to update an existing board.
   * GET boards/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({params, request, response, view}) {
  }

  /**
   * Update board details.
   * PUT or PATCH boards/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({params, request, response}) {
    let status = 200;
    let message = 'Successful update';
    let instance = {};
    response.statusCode = 404;
    try {
      instance = await Board.findByOrFail(index, params.id);
      instance.merge({...request.post()});
      await instance.save();
    } catch (e) {
      status = 500;
      ({message} = e);
    }
    return response.status(status).json(message);
  }

  /**
   * Delete a board with id.
   * DELETE boards/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({params, request, response}) {
    let status = 200;
    let message = 'Successful delete';
    response.statusCode = 404;
    try {
      const instance = await Board.findByOrFail(index, params.id);
      await instance.delete();
    } catch (e) {
      status = 500;
      ({message} = e);
    }
    return response.status(status).json(message);
  }
}

module.exports = BoardController;
