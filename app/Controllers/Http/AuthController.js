'use strict';
let User = use('App/Models/User');

class AuthController {
  async generateAPIToken({auth, params, request, response}) {
    let user = await User.findByOrFail(params);
    return await auth.authenticator('api').generate(user);
  }
}

module.exports = AuthController;
