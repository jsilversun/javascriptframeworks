/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/**
 * Resourceful controller for interacting with users
 */

'use strict';
/** @typedef {import('@adonisjs/framework/src/Request')} Request */

/** @typedef {import('@adonisjs/framework/src/View')} View */
let User = use('App/Models/User');
let index = 'username';

class UserController {
  /**
   * Show a list of all users.
   * GET users
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({request, response, view}) {
    return await User.all();
  }

  /**
   * Render a form to be used for creating a new user.
   * GET users/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({request, response, view}) {
  }

  /**
   * Create/save a new user.
   * POST users
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({request, response}) {
    let status = 200;
    let message = 'Successful insertion';
    response.statusCode = 404;
    try {
      await User.create({...request.post()});
    } catch (e) {
      status = 500;
      ({message} = e);
    }
    return response.status(status).json(message);
  }

  /**
   * Display a single user.
   * GET users/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({params, request, response, view}) {
    return await User.findByOrFail(index, params.id);
  }

  /**
   * Render a form to update an existing user.
   * GET users/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({params, request, response, view}) {
  }

  /**
   * Update user details.
   * PUT or PATCH users/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({params, request, response}) {
    let status = 200;
    let message = 'Successful update';
    let instance = {};
    response.statusCode = 404;
    try {
      instance = await User.findByOrFail(index, params.id);
      instance.merge({...request.post()});
      await instance.save();
    } catch (e) {
      status = 500;
      ({message} = e);
    }
    return response.status(status).json(message);
  }

  /**
   * Delete a user with id.
   * DELETE users/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({params, request, response}) {
    let status = 200;
    let message = 'Successful delete';
    response.statusCode = 404;
    try {
      const lang = await User.findByOrFail(index, params.id);
      await lang.delete();
    } catch (e) {
      status = 500;
      ({message} = e);
    }
    return response.status(status).json(message);
  }
}

module.exports = UserController;
