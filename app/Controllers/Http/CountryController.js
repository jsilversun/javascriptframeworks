'use strict';
const Language = use('App/Models/Language');
const Country = use('App/Models/Country');
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with countries
 */
let index = 'ISO';
class CountryController {
  /**
   * Show a list of all countries.
   * GET countries
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({request, response, view}) {
    return Country.all();
  }

  /**
   * Render a form to be used for creating a new country.
   * GET countries/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({request, response, view}) {
  }

  /**
   * Create/save a new country.
   * POST countries
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({request, response}) {
    let status = 200;
    let message = 'Successful insertion';
    let data = {...request.post()};
    response.statusCode = 404;
    try {
      let {languageId, ...countryData} = data;
      let language = await Language.findByOrFail('code', languageId);
      await language.countries().create(countryData);
    } catch (e) {
      status = 500;
      ({message} = e);
    }
    return response.status(status).json(message);
  }

  /**
   * Display a single country.
   * GET countries/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({params, request, response, view}) {
    return Country.findByOrFail(index, params.id);
  }

  /**
   * Render a form to update an existing country.
   * GET countries/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({params, request, response, view}) {
  }

  /**
   * Update country details.
   * PUT or PATCH countries/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({params, request, response}) {
    let status = 200;
    let message = 'Successful update';
    let instance = {};
    response.statusCode = 404;
    try {
      instance = await Country.findByOrFail(index, params.id);
      instance.merge({...request.post()});
      await instance.save();
    } catch (e) {
      status = 500;
      ({message} = e);
    }
    return response.status(status).json(message);
  }

  /**
   * Delete a country with id.
   * DELETE countries/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({params, request, response}) {
    let status = 200;
    let message = 'Successful delete';
    response.statusCode = 404;
    try {
      const country = await Country.findByOrFail(index, params.id);
      await country.delete();
    } catch (e) {
      status = 500;
      ({message} = e);
    }
    return response.status(status).json(message);
  }
}

module.exports = CountryController;
