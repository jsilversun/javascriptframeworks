'use strict';
const Category = use('App/Models/Category');
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with languages
 */
let index = 'icon';
class CategoryController {
  /**
   * Show a list of all languages.
   * GET languages
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({request, response, view}) {
    return Category.query().with('translations').fetch();
  }

  /**
   * Render a form to be used for creating a new language.
   * GET languages/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({request, response, view}) {
  }

  /**
   * Create/save a new language.
   * POST languages
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({request, response}) {
    let status = 200;
    let message = 'Successful insertion';
    response.statusCode = 404;
    try {
      await Category.create({...request.post()});
    } catch (e) {
      status = 500;
      ({message} = e);
    }
    return response.status(status).json(message);
  }

  /**
   * Display a single language.
   * GET languages/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({params, request, response, view}) {
    return Category.findByOrFail(index, params.id);
  }

  /**
   * Render a form to update an existing language.
   * GET languages/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({params, request, response, view}) {
  }

  /**
   * Update language details.
   * PUT or PATCH languages/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({params, request, response}) {
    let status = 200;
    let message = 'Successful update';
    let instance = {};
    response.statusCode = 404;
    try {
      instance = await Category.findByOrFail(index, params.id);
      instance.merge({...request.post()});
      await instance.save();
    } catch (e) {
      status = 500;
      ({message} = e);
    }
    return response.status(status).json(message);
  }

  /**
   * Delete a language with id.
   * DELETE languages/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({params, request, response}) {
    let status = 200;
    let message = 'Successful delete';
    response.statusCode = 404;
    try {
      const instance = await Category.findByOrFail(index, params.id);
      await instance.delete();
    } catch (e) {
      status = 500;
      ({message} = e);
    }
    return response.status(status).json(message);
  }
}

module.exports = CategoryController;
