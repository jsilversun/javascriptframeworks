'use strict';

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route');
Route.resource('languages', 'LanguageController').middleware('auth:api');
Route.resource('countries', 'CountryController');
Route.resource('categories', 'CategoryController');
Route.resource('boards', 'BoardController');
Route.resource('users', 'UserController');
Route.resource('maps', 'MapController');
Route.group(() => {
  Route.get('generateToken/:id', 'AuthController.generateToken');
}).prefix('auth');

