module.exports = function(options){
  const LoopBackContext = require('loopback-context');
  let languages = [];

  function evaluateLang(lang){
	return new Promise((resolve, reject) =>{
	  languages.includes(lang) ?
		  resolve(lang) :
		  reject({
			message: `The requested language it's not supported`,
			error: true,
			languages: languages
		  });
	});
  }

  function supportedLanguage(lang, app){
	if (languages.length) return evaluateLang(lang);

	return new Promise((resolve, reject) =>{
	  if (options.languages) {
		languages = options.languages;
		return evaluateLang(lang).then(resolve).catch(reject);
	  }

	  app.models.Language.find((err, langs) =>{
		if (err) throw err;

		languages = langs.map(language =>{
		  return language.code;
		});
		evaluateLang(lang).then(resolve).catch(reject);
	  });
	});
  }

  return function(req, res, next){
	supportedLanguage(req.get('language') || req.query.lang || options.default, req.app)
		.then(lang =>{
		  let ctx = LoopBackContext.getCurrentContext({bind: true});
		  if (ctx) {
			next = ctx.bind(next);
			ctx.set('lang', lang);
			ctx.set('languages', languages);
		  }

		  next();
		})
		.catch(err =>{
		  res.status(404).send(err);
		});
  };
};