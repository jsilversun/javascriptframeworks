module.exports = function(options){
  const LoopBackContext = require('loopback-context');

  return function storeCurrentUser(req, res, next){
	if (!req.accessToken) {
	  return next();
	}

	req.app.models.User.findById(req.accessToken.userId, function(err, user){
	  if (err) {
		return next(err);
	  }

	  if (!user) {
		return next(new Error('No user with this access token was found.'));
	  }

	  let loopbackContext = LoopBackContext.getCurrentContext();
	  if (loopbackContext) {
		loopbackContext.set('currentUser', {
		  id: user.id,
		  sub: user.sub,
		  refresh_token: user.refresh_token,
		  username: user.username,
		  email: user.email,
		  emailVerified: user.emailVerified
		});
	  }

	  next();
	});
  };
};