module.exports = function(options){
  const LoopBackContext = require('loopback-context');
  let paths = [];

  function supportedPath(path){
	return new Promise((resolve, reject) =>{
	  paths.some(target =>{
		return target.test(path);
	  }) ?
		  resolve(path) :
		  reject({
			message: `This path doesn't require google-auth`,
			error: true,
			code: 501
		  });
	});
  }

  function evaluatePath(path){
	if (paths.length) return supportedPath(path);

	return new Promise((resolve, reject) =>{
	  paths = options.paths.map((target =>{
		return new RegExp(target);
	  }));

	  supportedPath(path).then(resolve).catch(reject);
	});
  }

  function authUser(path){
	return new Promise((resolve, reject) =>{
	  evaluatePath(path)
		  .then(() =>{
			let ctx = LoopBackContext.getCurrentContext();
			let currentUser = ctx && ctx.get('currentUser');

			if (currentUser) {
			  //TODO: Do all the google-auth work to successfully login the user.
			  console.log('login');
			  return resolve(currentUser);
			}

			reject({
			  message: `The request did't provide auth credentials.`,
			  error: true,
			  code: 403
			})
		  })
		  .catch(err =>{
			if (err.code === 501) return resolve(err);
			reject(err);
		  });
	});
  }

  return function(req, res, next){
	authUser(req.path)
		.then(result =>{
		  if (result.error && result.code === 501) {
			return next();
		  }

		  next();
		})
		.catch(err =>{
		  if (err.code === 403) {
			return res.status(err.code).send(err);
		  }
		  next(err);
		});
  };
};