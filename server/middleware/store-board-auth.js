module.exports = function(options){
  const LoopBackContext = require('loopback-context');

  return function(req, res, next){
	req.app._boardCredentials(req.params.id)
		.then(result =>{
		  let ctx = LoopBackContext.getCurrentContext({bind: true});
		  if (ctx) {
			ctx.set('tableId', result.tableId);
			ctx.set('credentials', result.credentials);
			next = ctx.bind(next);
		  }

		  next();
		})
		.catch(err =>{
		  if (err.code === 404) {
			return res.status(err.code).send(err);
		  }

		  console.error(err);
		  next(err);
		});
  };
};