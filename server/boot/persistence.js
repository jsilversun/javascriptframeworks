module.exports = function (app) {

  /**
   * @persistence
   * Logic 4 migrate or update DATA
   *
   * @param {string} email - email of a certain user already registered on RankCountry.
   *
   *
   * TODO only receive petitions from local server... (common or server stuff)
   */

  /* Information about app and loopback
   * Roy: The `app` object provides access to a variety of LoopBack resources such as
   * models (e.g. `app.models.YourModelName`) or data sources (e.g.`app.datasources.YourDataSource`).
   * See
   * http://docs.strongloop.com/display/public/LB/Working+with+LoopBack+objects
   * for more info.
   *
   * E.G. console.log(app.dataSources.RCdatabase.settings);
   */
  /*
   *
   * Other functions that could be useful
   * 2 migrate or update EVERYTHING
   *
   */

  function autoUpdateAll() {
    Object.keys(models).forEach(function (key) {
      if (typeof models[key].dataSource != 'undefined') {
        if (typeof datasources[models[key].dataSource] != 'undefined') {
          app.dataSources[models[key].dataSource].autoupdate(key, function (err) {
            if (err) throw err;
            console.log('Model ' + key + ' updated');
          });
        }
      }
    });
  }

  function autoMigrateAll() {
    Object.keys(models).forEach(function (key) {
      if (typeof models[key].dataSource != 'undefined') {
        if (typeof datasources[models[key].dataSource] != 'undefined') {
          app.dataSources[models[key].dataSource].automigrate(key, function (err) {
            if (err) throw err;
            console.log('Model ' + key + ' migrated');
          });
        }
      }
    });
  }

};
