module.exports = function(app){
  const Cache = require('js-cache');
  const credentialsCache = new Cache();
  const locales = require('../../common/assets/locales');

  app.locales = locales;
  app._boardCredentials = async (id) =>{
	let credentials = credentialsCache.get(`board-${id}-credentials`);
	if (credentials) return credentials;

	return new Promise((resolve, reject) =>{
	  const query = ['SELECT u.sub, u.refresh_token, u.email, u.username, u.emailVerified, m.tableId'];
	  query.push('FROM Board b');
	  query.push('JOIN Map m ON m.id = b.Map_id');
	  query.push('JOIN user u ON u.id = m.user_id');
	  query.push('WHERE b.id = ?');

	  app.datasources['RCdatabase'].connector.execute(query.join(' '), [id], function(err, result){
		if (err) return reject(err);

		result = result[0];
		if (result) {
		  credentials = Object.assign({}, result);
		  delete credentials.tableId;

		  result = {credentials: credentials, tableId: result.tableId};
		  credentialsCache.set(`board-${id}-credentials`, result, 60 * 60 * 1000);
		  return resolve(result);
		}

		return reject({
		  result: 0,
		  message: "BOARD NOT FOUND IN RANK COUNTRY",
		  code: 404,
		  error: true
		})
	  });
	});
  }
};
