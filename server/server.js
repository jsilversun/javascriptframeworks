require('cls-hooked');
const loopback = require('loopback');
const boot = require('loopback-boot');
const https = require('https');
const http = require('http');
const app = module.exports = loopback();

app.start = function(httpOnly){
  if (httpOnly === undefined) {
	httpOnly = process.env.HTTP;
  }
  let server = null;
  if (!httpOnly) {
	const sslConfig = require('./ssl-config');
	server = https.createServer({
	  key: sslConfig.privateKey,
	  cert: sslConfig.certificate
	}, app);
  } else {
	server = http.createServer(app);
  }

  server.listen(app.get('port'), function(){
	let baseUrl = (httpOnly ? 'http://' : 'https://') + app.get('host') + ':' + app.get('port');
	app.emit('started', baseUrl);
	console.log('----> Rank Country Web server listening ---->: %s', baseUrl);
	if (app.get('loopback-component-explorer')) {
	  var explorerPath = app.get('loopback-component-explorer').mountPath;
	  console.log('----> Rank Country REST API ----> %s%s', baseUrl, explorerPath);
	}
  });
  return server;
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err){
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module)
	app.start();
});
