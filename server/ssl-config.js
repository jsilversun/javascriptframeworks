var path = require('path');
var fs = require('fs');
exports.privateKey = fs.readFileSync(path.join('/etc/letsencrypt/live/api.rankcountry.com/privkey.pem')).toString();
exports.certificate = fs.readFileSync(path.join('/etc/letsencrypt/live/api.rankcountry.com/fullchain.pem')).toString();
