'use strict';

module.exports = function(Language){
  const LoopBackContext = require('loopback-context');
  const secrets = require('../../server/dev_google_clientid.json'); // RankCountry clientId
  const Translate = require('@google-cloud/translate');
  const translate = new Translate({
	projectId: secrets.web.project_id,
	key: secrets.web.key
  });
  const languages = [];
  const maxCharacters = 5000;
  const limit = {count: 100000, ms: 1000 * 100};
  let count, nextStop;

  Language.remoteMethod('locales', {
	description: 'Get current tags available',
	http: {path: '/locales', verb: 'get'},
	returns: {arg: 'translation', root: true, type: 'Object'}
  });
  Language.locales = function(callback){
	callback(null, Language.app.locales);
  };

  Language.remoteMethod('translateQueries', {
	description: 'Translate body',
	http: {path: '/translate', verb: 'post'},
	accepts: {
	  arg: 'origins',
	  type: 'Object',
	  required: true,
	  http: {source: 'body'}
	},
	returns: {arg: 'translation', type: 'Object'}
  });
  Language.translateQueries = function(origins, callback){
	let ctx = LoopBackContext.getCurrentContext();
	let lang = ctx && ctx.get('lang');
	Language.translate(origins, origins.targets || [lang])
		.then(results =>{
		  callback(null, results);
		})
		.catch(callback);
  };

  Language.translate = function(origins, languages = languages){
	if (typeof languages === 'string') languages = [languages];
	if (!nextStop || Date.now() >= nextStop) {
	  nextStop = Date.now() + limit.ms;
	  count = 0;
	}

	return Promise.all(languages.map(async (lang) =>{
	  try {
		let q = [];
		for (const [query, length, index]of stepsGenerator(maxCharacters, origins.queries.concat())) {
		  count += length;
		  if (count >= limit.count) {
			const msWait = nextStop - Date.now();
			console.log(`User limit reached, waiting for ${msWait / 1000}s until limit get purged.`);
			nextStop = await _wait(msWait) + limit.ms;
			count = length;
		  }

		  console.log(`for ${lang}, the #${index} request is ${length} length.\t\tCurrent count: ${count}`);
		  q = q.concat((await translate.translate(query, {to: lang, from: origins.lang}))[0]);
		}

		return {from: origins.lang, lang: lang, queries: q};
	  } catch (e) {
		return e;
	  }
	}));
  };

  function* stepsGenerator(max, queries = []){
	let step, count, query, index = 0;
	while (queries.length) {
	  step = count = 0;
	  queries.find(query =>{
		step++;
		count += query.length;
		if (count > max) {
		  step--;
		}

		return count >= max;
	  });

	  query = queries.splice(0, step);
	  yield [query, query.join('').length, index++];
	}
  }

  /***
   * Wait Promise
   * @param timeout (miliseconds)
   * @returns {Promise<any>}
   * @private
   */
  function _wait(timeout){
	return new Promise(resolve => setTimeout(() => resolve(Date.now()), timeout));
  }
};