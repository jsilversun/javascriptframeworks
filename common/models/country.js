module.exports = function(Country){
  const LoopBackContext = require('loopback-context');
  const countriesCache = require('js-cache');
  const request = require('request');

  /**
   * @andFlags
   * @desc
   * Returns countries info extracted from RestCountries.eu API
   *
   */
  Country.remoteMethod('andFlags', {
	description: 'andFlags',
	http: {path: '/andFlags', verb: 'get'},
	returns: {arg: 'andFlags', type: 'Object'}
  });
  Country.andFlags = function(cb){
	let ctx = LoopBackContext.getCurrentContext();
	let lang = ctx && ctx.get('lang');
	let allCountries = countriesCache.get(lang);
	if (allCountries) {
	  return cb(null, allCountries);
	}

	request.get('https://restcountries.eu/rest/v1/all', function(err, countries){
	  if (err) {
		console.error(err);
		return cb(err);
	  }

	  allCountries = {};
	  JSON.parse(countries.body).map(function(country){
		allCountries[country.alpha2Code] = {name: lang === 'en' ? country.name : country.translations[lang] || country.name};
		return country;
	  });

	  countriesCache.set(lang, allCountries, 72 * 60 * 60 * 1000);
	  cb(null, allCountries);
	});
  };
};
