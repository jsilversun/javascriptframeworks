/**
 * Copyright 2016 Rank Country All Rights Reserved.
 *
 * Rank Country Backend > Role Model!
 * Roles for users, remote methods by Rol.
 *
 */

module.exports = function(Role){
  // Google Libraries
  const {google} = require('googleapis');
  const secrets = require('../../server/dev_google_clientid.json'); // RankCountry clientId
  // OAuth2 Client that handles the authorization of API's
  const oauth2Client = new google.auth.OAuth2(
	  secrets.web.client_id,
	  secrets.web.client_secret,
	  secrets.web.REDIRECT_URL || 'postmessage'
  );
  const usersCache = require('js-cache');

  /**
   * RemoteMethods Loopback Declarations/Configurations/Definitions
   */

  /**
   * @oauth2callback
   * @desc
   * 1. Validates an Owner account(that are the ones that can provide DATA maps and categories to the application)
   * 2. Then, stores the refresh_token into the database for later usage.
   *
   * In the actual version of RankCountry:
   * 1. Only certain users can upload maps. So taking this into account this "Mapper/Owner/Admin" Roles need to be created on loopback first.
   * If they are registered in Loopback then system will store their sub and refresh_token
   *
   * For the future is possible to create a user as a client after they signIn with Google here.
   *
   * @param {string} code - Unique Code returned by Google for a certain user.
   * @param {object} cb - Callback.
   *
   * TODO Return structure...
   * Positive case: {accessToken: '//accessToken'}
   * Negative case: {message: '//message'}
   *
   */
  Role.remoteMethod('oauth2callback', {
	description: 'Server-Side Google Authentication and Authorization for RankCountry.',
	accepts: {arg: "code", type: "String", required: true},
	http: {path: '/oauth2callback', verb: 'post'},
	returns: {arg: 'oauth2callback', type: 'String'}
  });
  Role.oauth2callback = function(code, callback){
	(async () =>{
	  try {
		const {tokens} = await oauth2Client.getToken(code);
		const tokenDataSegments = tokens.id_token.split('.');
		const payloadData = JSON.parse(oauth2Client.decodeBase64(tokenDataSegments[1]));
		let user = (await Role.app.models.user.find({where: {email: payloadData.email}, limit: 1}))[0];

		if (user) { // User exists
		  user.sub = payloadData.sub;
		  user.emailVerified = payloadData.email_verified;
		  user.refresh_token = tokens.refresh_token;
		  const role = await Role.app.models.RoleMapping.findOne({
			where: {
			  principalType: 'USER',
			  principalId: user.id
			}
		  });

		  const login = await Role.app.models.user.login({email: payloadData.email, password: "g23++"});
		  user = await Role.app.models.user.upsert(user);

		  console.log(new Date() + '. ' + (role.roleId === 1 || role.roleId === 2 ? 'Admin' : 'Client') + ' has logged in');
		  console.log('Username: ' + user.username + ', email:' + user.email + ', just loggedIn');

		  usersCache.del(`user-${user.sub}`);
		  return {accessToken: login['id']};
		}
		else {
		  console.log('User: ' + payloadData.email + ', is not registered in RankCountry.');
		  return {message: 'User is not registered in RankCountry.'};
		}
	  } catch (e) {
		console.error(e);
		throw e;
	  }
	})().then(result => callback(null, result))
		.catch(callback);
  };

  Role.remoteMethod('createAdminUser', {
	description: 'Create a new Admin User on RankCountry',
	accepts: {arg: "userInfo", type: "Object", required: true},
	http: {path: '/createAdminUser', verb: 'post'},
	returns: {arg: 'createAdminUserResult', type: 'Object'}
  });
  Role.createAdminUser = function(userInfo, cb){
	const newUser = {
	  email: userInfo.email,
	  password: 'g23++',
	  status: 'Awesome',
	  username: userInfo.username,
	  challenge: {},
	  credentials: {},
	  emailVerified: 0,
	  created: new Date(),
	  lastUpdated: new Date()
	};

	Role.app.models.user.beginTransaction({isolationLevel: Role.app.models.User.Transaction.READ_COMMITTED}, function(errTX, tx){
	  if (errTX) {
		console.error(errTX);
		return cb(errTX);
	  }
	  Role.app.models.User.create(newUser, {transaction: tx}, function(errUserCreation, userInstance){
		if (errUserCreation) {
		  tx.rollback(function(errRollback){
			if (errRollback) {
			  console.error(errRollback);
			}
			cb(errUserCreation);
		  });
		  return console.error(errUserCreation)
		}
		Role.findById(1, function(errorFind, rol){
		  if (errorFind) return console.error(errorFind);
		  rol.principals.create({
			principalType: Role.app.models.RoleMapping.USER,
			principalId: userInstance.id
		  }, {transaction: tx}, function(errorRol, rol){
			if (errorRol) {
			  tx.rollback(function(errRollback){
				if (errRollback) {
				  console.error(errRollback);
				}
				cb(errorRol);
			  });
			  return console.error(errorRol)
			}
			tx.commit(function(errCommit){
			  if (errCommit) {
				console.error(errCommit);
				return cb(errCommit);
			  }
			  cb(null, userInstance);
			});
		  });
		});
	  });
	});
  }
};
