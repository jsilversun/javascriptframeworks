module.exports = function(Category){
  const LoopBackContext = require('loopback-context');
  const categoriesCache = require('js-cache');

  /**
   * @menuList
   * Menu List DATA
   * Returns Categories info plus an array of maps on each one.
   *
   */
  Category.remoteMethod('menuList', {
	description: 'Returns Categories info plus an array of maps on each one.',
	http: {path: '/menuList', verb: 'get'},
	returns: {arg: 'menuList', type: 'Object'}
  });
  Category.menuList = function(callback){
	(async () =>{
	  try {
		const ctx = LoopBackContext.getCurrentContext();
		const lang = ctx && ctx.get('lang');
		const currentUser = ctx && ctx.get('currentUser');
		let list;
		if (!currentUser) {
		  list = categoriesCache.get(`menu-list-${lang}`);
		  if (list) return list;
		}

		list = [];
		const query = [];
		query.push('SELECT *');
		query.push('FROM categories');
		query.push('WHERE code = ?');
		query.push('ORDER BY name, position');

		let boards = await _executeQuery(query.join(' '), lang);
		let currentCategory = {};
		boards.forEach(row =>{
		  if (currentCategory.id !== row.category) {
			currentCategory = {
			  id: row.category,
			  name: row.name,
			  description: row.description,
			  icon: row.icon,
			  boards: []
			};

			list.push(currentCategory);
		  }

		  currentCategory.boards.push(row.board);
		});

		if (currentUser) {
		  query.length = 0;
		  query.push('SELECT');
		  query.push('C.id, CL.name, CL.description, C.icon');
		  query.push('FROM Category C');
		  query.push('JOIN CryLge CL on C.id = CL.Category_id');
		  query.push('WHERE CL.Language_code = CL.default_lge');
		  query.push('AND C.id NOT IN');
		  query.push('(SELECT Category_id FROM BrdCry group by Category_id)');
		  query.push('ORDER BY name');

		  (await _executeQuery(query.join(' '), lang)).forEach(category =>{
			category.boards = [];
			list.push(category)
		  });
		  list.sort((a, b) => a.name.toUpperCase().localeCompare(b.name.toUpperCase()));
		}

		query.length = 0;
		query.push('SELECT');
		query.push('B.id');
		query.push('FROM Board B');
		query.push('WHERE B.id NOT IN');
		query.push('(SELECT Board_id FROM BrdCry GROUP BY Board_id)');

		boards = await _executeQuery(query.join(' '));
		boards.length && list.push({name: Category.app.locales[lang].label_not_Category, icon: 'list', boards: boards.map(board => board.id)});

		!currentUser && categoriesCache.set(`menu-list-${lang}`, list, 8 * 60 * 60 * 1000);
		return list;
	  } catch (e) {
		console.error(e);
		throw e;
	  }
	})()
		.then(result =>{
		  callback(null, result);
		})
		.catch(callback);
  };

  /**
   * @upsertCategory
   *
   * Creates a new category.
   *
   */
  Category.remoteMethod('upsertCategory', {
	description: 'Create a new category with related maps at once.',
	accepts: {
	  arg: '{name, description, icon, boards[]}',
	  type: 'object',
	  required: true,
	  http: {source: 'body'}
	},
	http: {path: '/upsert', verb: 'post'},
	returns: {
	  arg: 'upsertCategory',
	  type: 'Object'
	}
  });
  Category.upsertCategory = function(newCat, callback){
	(async () =>{
	  const ctx = LoopBackContext.getCurrentContext();
	  const lang = ctx && ctx.get('lang');
	  const tx = await Category.beginTransaction({isolationLevel: Category.app.models.Map.Transaction.READ_COMMITTED});

	  try {
		newCat.languageCode = newCat.defaultLge = lang;
		newCat.description = newCat.description && newCat.description.trim() !== '' ? newCat.description : 'N/A';
		const toUpdate = newCat.boards;
		delete newCat.boards;

		const category = await Category.upsert(newCat, {transaction: tx});
		newCat.categoryId = category.id;
		const catData = await Category.app.models.CryLge.upsert(newCat, {transaction: tx});
		const toDelete = await _executeFind(category.brdcries, {transaction: tx});
		if (toDelete) {
		  await Promise.all(toDelete
			  .filter(board => toUpdate.indexOf(board.boardId) === -1)
			  .map(board => board.delete({transaction: tx}))
		  );
		}

		await Promise.all(toUpdate.map((board, i) =>{
		  return Category.app.models.BrdCry.upsert({
			"categoryId": category.id,
			"boardId": board,
			"position": i + 1
		  }, {transaction: tx});
		}));
		await tx.commit();
		_flushCache(/user-\w+$/);
		return {message: 'Category: ' + catData.name + ' was successfully upserted'};
	  } catch (e) {
		await tx.rollback();
		console.error(e);
		throw e;
	  }
	})()
		.then(result =>{
		  callback(null, result);
		})
		.catch(callback);

	//{
	// "name": "Test",
	// "description": "   ",
	// "icon":"list",
	// "boards":[123,51,12]
	// }
  };

  /**
   * @deleteCategory
   *
   * Delete a category.
   *
   */
  Category.remoteMethod('deleteCategory', {
	description: 'Delete a category.',
	accepts: {
	  arg: 'id',
	  type: 'Number',
	  required: true,
	  http: {source: 'body'}
	},
	http: {path: '/delete', verb: 'post'},
	returns: {
	  arg: 'deleteCategory',
	  type: 'Object'
	}
  });
  Category.deleteCategory = function(categoryId, callback){
	(async () =>{
	  const tx = await Category.beginTransaction({isolationLevel: Category.app.models.Map.Transaction.READ_COMMITTED});

	  try {
		await Category.deleteById(categoryId, {transaction: tx});

		await tx.commit();
		_flushCache(/user-\w+$/);
		return {message: 'Category: ' + categoryId + ' was successfully removed'}
	  } catch (e) {
		await tx.rollback();
		console.error(e);
		throw e;
	  }
	})()
		.then(result =>{
		  callback(null, result);
		})
		.catch(callback);
  };

  Category.remoteMethod('translateCategories', {
	description: 'Translate category',
	http: {path: '/translateCategories', verb: 'get'},
	returns: {arg: 'categoryTranslated', type: 'Object'}
  });
  Category.translateCategories = function(callback){
	(async () =>{
	  const tx = await Category.beginTransaction({isolationLevel: Category.app.models.Map.Transaction.READ_COMMITTED});

	  try {
		const origins = {};
		const categoryList = {};
		const properties = ['name', 'description'];
		const query = [];
		query.push(`SELECT category id, code as 'to', lang as 'from', name, description`);
		query.push('FROM categories');
		query.push('WHERE code NOT LIKE lang');
		query.push('GROUP BY category, code');

		const results = await _executeQuery(query.join(' '), [], {transaction: tx});
		results.forEach(result =>{
		  categoryList[result.id] = categoryList[result.id] || {};
		  categoryList[result.id][result.to] = _mapProperties(result, properties);
		  categoryList[result.id][result.to].defaultLge = result.from;

		  let target = origins[result.from] || {};
		  let categoryIds = target[result.to] || [];
		  categoryIds.push(result.id);
		  target[result.to] = categoryIds;
		  origins[result.from] = target;
		});

		let translations = [];
		Object.keys(origins).forEach(from =>{
		  Object.keys(origins[from]).forEach(to =>{
			let queries = [];
			let i = 0;
			origins[from][to].forEach((category) =>{
			  properties.forEach(prop =>{
				queries.push(categoryList[category][to][prop]);
				categoryList[category][to][prop] = i++;
			  });
			});

			translations.push(Category.app.models.Language.translate({lang: from, queries: queries}, to));
		  });
		});

		translations = await Promise.all(translations);
		let upserts = [];
		const categories = Object.keys(categoryList);

		translations.forEach(translation =>{
		  translation = translation[0];
		  categories.forEach(id =>{
			let category = categoryList[id][translation.lang];
			if (category) {
			  category.categoryId = id;
			  category.languageCode = translation.lang;
			  properties.forEach(prop =>{
				category[prop] = translation.queries[category[prop]];
			  });

			  upserts.push(Category.app.models.CryLge.upsert(category, {transaction: tx}));
			}
		  });
		});

		upserts = await Promise.all(upserts);

		await tx.commit();
		return {message: 'Some categories were translated', translations: upserts}
	  } catch (e) {
		await tx.rollback();
		console.error(e);
		throw e;
	  }
	})()
		.then(result =>{
		  callback(null, result);
		})
		.catch(callback);
  };

  function _mapProperties(obj, properties){
	let newObj = {};
	properties.forEach(prop =>{
	  newObj[prop] = obj[prop];
	});

	return newObj;
  }

  /***
   * Execute a Query using Promises
   * @param query
   * @param queryParams
   * @param options
   * @returns {Promise<any>}
   * @private
   */
  function _executeQuery(query, queryParams, options){
	return new Promise((resolve, reject) =>{
	  queryParams = queryParams && !Array.isArray(queryParams) ? [queryParams] : queryParams;
	  Category.app.datasources['RCdatabase'].connector.execute(query, queryParams, options, (queryError, results) =>{
		if (queryError) return reject(queryError);
		resolve(results);
	  });
	});
  }

  /***
   * Execute a value resolution using Promises
   * @param func
   * @param params
   * @returns {Promise<any>}
   * @private
   */
  function _executeFind(func, params){
	if (typeof func !== 'function') throw new Error('the first params must be a function');

	return new Promise((resolve, reject) =>{
	  func(params, (error, results) =>{
		if (error) return reject(error);
		resolve(results);
	  });
	});
  }

  async function _flushCache(exclude){
	let keys = categoriesCache.keys();
	if (Array.isArray(exclude)) keys = keys.filter(key => exclude.indexOf(key) === -1);
	if (exclude instanceof RegExp) keys = keys.filter(key => !exclude.test(key));

	return await Promise.all(keys.map(async key => Object.assign({name: key, flushed: categoriesCache.del(key)})));
  }
};