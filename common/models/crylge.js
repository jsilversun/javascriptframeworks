'use strict';

module.exports = function(CryLge){

  CryLge.upsertCryLge = function(category, options = {}){
	return new Promise((resolve, reject) =>{
	  CryLge.upsert(category, options, function(err, category){
		if (err) return reject(err);
		resolve(category);
	  });
	});
  }
};
