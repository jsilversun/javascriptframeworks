module.exports = function(Board){
  const moment = require('moment');
  const numeral = require('numeral');
  const boardCache = require('js-cache');
  const LoopBackContext = require('loopback-context');

  // Google Libraries
  const {google} = require('googleapis');
  const secrets = require('../../server/dev_google_clientid.json'); // RankCountry clientId
  const fusiontables = google.fusiontables('v2');
  // OAuth2 Client that handles the authorization of API's
  const oauth2Client = new google.auth.OAuth2(
	  secrets.web.client_id,
	  secrets.web.client_secret,
	  secrets.web.REDIRECT_URL || 'postmessage'
  );

  const btoa = require('btoa'); // Binary to ASCII
  const statistic = require('simple-statistics'); //Statistic Lib
  const Rainbow = require('rainbowvis.js'); // Color helper Lib
  const legendKeyName = 'Legend Key';
  const formatPatters = require('../assets/format-patterns');

  //Private functions
  /**
   * @_querySerializer
   * @desc
   * Fusion Tables query Serializer
   *
   * @param {string} tableId
   * @param {array} columns
   * @param {array} order.
   * @param {array} group.
   *
   * @returns {string}
   */
  function _querySerializer(tableId, columns, order, group){
	if (!tableId && tableId === '') {
	  return '';
	}

	var joinFormatter = function(array, encapsulation, separator){
	  var enc = encapsulation ? encapsulation : '';
	  var sep = separator ? separator : '+';

	  return array ? enc + array.map(function(item){
		return item.columnId ? 'col' + item.columnId : item.name;
	  }).join(enc + sep + enc) + enc : '';
	};

	var query = ['SELECT'];
	query.push((columns ? joinFormatter(columns, '\'', ',') : '*'));
	query.push('FROM');
	query.push(tableId);

	if (order) {
	  query.push('ORDER');
	  query.push('BY');
	  query.push(joinFormatter(order.columns, '\'', ','));
	  query.push(order.direction);
	}

	if (group) {
	  query.push('GROUP');
	  query.push('BY');
	  query.push(joinFormatter(group, '\'', ','));
	}

	return query.join(' ');
  }

  /**
   * @_stylesRequest
   * @desc
   * Fusion Tables styles and style request
   *
   * @param {string} tableId
   * @param {int} styleId
   * @param {Object} auth
   *
   */
  async function _stylesRequest(tableId, styleId, auth){
	let result;
	if (styleId && styleId !== '') {
	  const {data} = await fusiontables.style.get({
		tableId: tableId,
		styleId: styleId,
		fields: 'markerOptions,name,polygonOptions,polylineOptions,styleId',
		userId: 'me',
		maxResults: 20,
		auth: auth || oauth2Client
	  });

	  result = data;
	} else {
	  const {data} = await fusiontables.style.list({
		tableId: tableId,
		fields: 'items(name,styleId)',
		userId: 'me',
		maxResults: 20,
		auth: auth || oauth2Client
	  });

	  result = data.items.map(style => Object.assign({
		id: style.styleId,
		name: style.styleId + ' - ' + (style.name || 'styleId')
	  }));
	}

	return result;
  }

  /**
   * @_templatesRequest
   * @desc
   * Fusion Tables templates and template request
   *
   * @param {string} tableId
   * @param {int} templateId
   * @param {Object} auth
   */
  async function _templatesRequest(tableId, templateId, auth){
	let result;
	if (templateId && templateId !== '') {
	  const {data} = await fusiontables.template.get({
		tableId: tableId,
		templateId: templateId,
		fields: 'automaticColumnNames,body,name,templateId',
		userId: 'me',
		maxResults: 20,
		auth: auth || oauth2Client
	  });

	  result = data;
	} else {
	  const {data} = await fusiontables.template.list({
		tableId: tableId,
		fields: 'items(name,templateId)',
		userId: 'me',
		maxResults: 20,
		auth: auth || oauth2Client
	  });

	  result = data.items.map(function(template){
		return {id: template.templateId, name: template.templateId + ' - ' + (template.name || 'templateId')};
	  });
	}

	return result;
  }

  /**
   * @_stylesTemplatesRequest
   * @desc
   * Fusion Tables styles and templates request
   *
   * @param {string} tableId
   * @param {Object} auth
   *
   */
  async function _stylesTemplatesRequest(tableId, auth){
	if (!tableId && tableId === '') throw new Error(`tableId it's required`);

	const results = await Promise.all([_stylesRequest(tableId, undefined, auth), _templatesRequest(tableId, undefined, auth)]);
	return {styles: results[0], templates: results[1]};
  }

  /**
   * @_legendByStyle
   * @desc
   * Legend board generation using Fusion Table style values
   *
   * @param {string} tableId
   * @param {int} styleId
   * @param {Array} data
   * @param {Array} legendKeys
   * @param {Object} auth
   */
  async function _legendByStyle(tableId, styleId, data, legendKeys, auth){
	if (!tableId || !styleId) throw new Error('tableId and styleId Required');

	const style = await _stylesRequest(tableId, styleId, auth);
	let legend = {filter: {}};
	let options = style.polygonOptions;

	if (options.fillColorStyler) {
	  options = options.fillColorStyler;
	  legend.filter.name = options.columnName;
	  switch (options.kind) {
		case'fusiontables#gradient':
		  options = options.gradient;
		  let jumps = (options.max - options.min) / options.colors.length;

		  legend.intervals = options.colors.map(function(color, i){
			color.min = options.min + i * jumps;
			color.max = color.min + jumps;
			return color;
		  });
		  break;
		case'fusiontables#buckets':
		  legend.intervals = options.buckets;
		  break;
	  }
	} else {
	  let rainbow = new Rainbow();
	  rainbow.setSpectrum('white', options.fillColor);
	  if (data) {
		let breaks = statistic.equalIntervalBreaks(data, 1 + 3.3 * Math.log10(data.length));
		let breakSize = breaks[1] - breaks[0];
		rainbow.setNumberRange(breaks[0] - breakSize, breaks[breaks.length - 1]);
		legend.intervals = breaks.map(function(breakPoint, i){
		  return {
			min: breakPoint - breakSize,
			max: breakPoint,
			color: rainbow.colourAt(breakPoint)
		  }
		});
		legend.intervals.shift();
	  }
	}

	if (legendKeys) {
	  legend.intervals.forEach(function(interval){
	    let indexLegend = data.findIndex((legendValue) => legendValue >= interval.min && legendValue <= interval.max);
		if(indexLegend > -1){
		  interval.legend = legendKeys[indexLegend] || '';
		  interval.isLegend = true;
		}
	  });
	}

	return legend;
  }

  //Public functions
  /**
   * @addBoard
   * Returns Categories info plus an array of maps on each one.
   *
   * @param {Object} addBoard
   *
   * 1. From Input    ->  Create Map
   * 2. From Both     ->  Create Board
   * 3. From Fusion   ->  Create Column
   * 4. From Both     ->  Create BrdCmn
   * 5. From Input    ->  Create BrdCry
   *
   */
  Board.remoteMethod('addBoard', {
	description: 'Create a new board with related categories at once.',
	accepts: {
	  arg: 'Map{tableId}, Board{name, description, styleId, templateId}, Columns[], categories[]}',
	  type: 'object',
	  required: true,
	  http: {source: 'body'}
	},
	http: {path: '/add', verb: 'post'},
	returns: {arg: 'addBoard', type: 'Object'}
  });
  Board.addBoard = function(newBoard, callback){
	(async () =>{
	  const ctx = LoopBackContext.getCurrentContext();
	  const currentUser = ctx && ctx.get('currentUser');
	  const lang = ctx && ctx.get('lang');
	  const tx = await Board.beginTransaction({isolationLevel: Board.app.models.Map.Transaction.READ_COMMITTED});

	  try {
		const adminMap = boardCache.get(`user-${currentUser.sub}`).find(table => table.id === newBoard.board.id);
		if (!adminMap) return new Error(`The board you are trying to add it's not within your current session`);
		delete newBoard.board.id;
		newBoard.board.languageCode = lang;
		newBoard.board.defaultLge = lang;

		const map = await Board.app.models.Map.create({
		  userId: currentUser.id,
		  tableId: adminMap.tableId
		}, {transaction: tx});

		const board = await map.boards.create(newBoard.board, {transaction: tx});
		await board.brdlges.create(newBoard.board, {transaction: tx});
		await Promise.all(newBoard.categories.map(catId => board.brdcries.create({categoryId: catId}, {transaction: tx})));
		await Promise.all(adminMap.columns.map(async fusionColumn =>{
		  fusionColumn.languageCode = lang;
		  delete fusionColumn.id;
		  const column = await map.columns.create(fusionColumn, {transaction: tx});
		  const brdcmn = newBoard.columns.find(bdrcmn => bdrcmn.columnId === column.columnId);

		  if (brdcmn) {
			brdcmn.columnId = column.id;
			brdcmn.isLegend = fusionColumn.name === legendKeyName;
			return board.brdcmns.create(brdcmn, {transaction: tx});
		  }
		}));

		await tx.commit();
		_flushCache();
		return {message: `The new board ${newBoard.board.name} was created`, board: board}
	  } catch (e) {
		await tx.rollback();
		console.error(e);
		throw e;
	  }
	})()
		.then(result =>{
		  callback(null, result);
		})
		.catch(callback);
  };

  /**
   * deleteBoard
   * @desc
   * Delete a board
   *
   * @param {Object} deleteBoard
   *
   */
  Board.remoteMethod('deleteBoard', {
	description: 'Delete a board with related categories at once.',
	accepts: {arg: 'boardId', type: 'Number', required: true},
	http: {path: '/delete', verb: 'post'},
	returns: {arg: 'deleteBoard', type: 'Object'}
  });
  Board.deleteBoard = function(boardId, callback){
	(async () =>{
	  const tx = await Board.beginTransaction({isolationLevel: Board.app.models.Map.Transaction.READ_COMMITTED});

	  try {
		const board = await Board.findById(boardId, {transaction: tx});
		await Board.app.models.Map.deleteById(board.mapId, {transaction: tx});

		await tx.commit();
		_flushCache();
		return {message: 'Board -' + board.id + '- Successfully deleted'}
	  } catch (e) {
		await tx.rollback();
		console.error(e);
		throw e;
	  }
	})()
		.then(result =>{
		  callback(null, result);
		})
		.catch(callback);
  };

  /**
   * @updateBoard
   * update a given Board
   *
   * @param {Object} board2update
   *
   */
  Board.remoteMethod('updateBoard', {
	description: 'Update a board with related categories at once.',
	accepts: {
	  arg: 'Map{tableId}, Board{name, description, styleId, templateId}, Columns[], categories[]}',
	  type: 'object',
	  required: true,
	  http: {source: 'body'}
	},
	http: {path: '/updating', verb: 'post'},
	returns: {arg: 'updateBoard', type: 'Object'}
  });
  Board.updateBoard = function(board2update, callback){
	(async () =>{
	  const tx = await Board.beginTransaction({isolationLevel: Board.app.models.Map.Transaction.READ_COMMITTED});

	  try {
		board2update.board.mapId = (await Board.findById(board2update.board.id, {transaction: tx})).mapId;
		const board = await Board.upsert(board2update.board, {transaction: tx});
		const map = await _executeFind(board.map, {transaction: tx});
		await Promise.all([
		  updateCategories(board2update.categories, board.brdcries),
		  updateColumns(board2update.columns, board.brdcmns, map.columns),
		  updateLanguage(board2update.board, board.brdlges)
		]);

		async function updateCategories(toUpdate, currentValues){
		  currentValues = await _executeFind(currentValues, {transaction: tx});
		  if (currentValues) {
			await Promise.all(currentValues
				.filter(category => toUpdate.indexOf(category.categoryId) === -1)
				// TODO: Find out why this is deleting all the records for the category instead of only the referenced one.
				//.map(category => category.delete({transaction: tx}))
				.map(category => _executeQuery('DELETE FROM BrdCry WHERE Category_id=? AND Board_id=?', [
				  category.categoryId,
				  board.id
				], {transaction: tx}))
			);
		  }

		  return Promise.all(toUpdate.map(category =>{
			if (currentValues.find(cat => cat.categoryId === category)) return true;

			return Board.app.models.BrdCry.create({
			  categoryId: category,
			  boardId: board.id
			}, {transaction: tx})
		  }));
		}

		async function updateColumns(toUpdate, currentValues, all){
		  const columnIds = toUpdate.map(col => col.columnId);
		  currentValues = await _executeFind(currentValues, {transaction: tx});
		  currentValues = await Promise.all(currentValues.map(async brdcmn =>{
			brdcmn._column = await _executeFind(brdcmn.column, {transaction: tx});
			return brdcmn;
		  }));

		  if (currentValues.length) {
			await Promise.all(currentValues
				.filter(brdcmn => columnIds.indexOf(brdcmn._column.columnId) === -1)
				.map(brdcmn => brdcmn.delete({transaction: tx}))
			);
		  }

		  all = await _executeFind(all, {transaction: tx});
		  return Promise.all(toUpdate.map(async brdcmn =>{
			const column = all.find(col => col.columnId === brdcmn.columnId);
			brdcmn.boardId = board.id;
			brdcmn.columnId = column.id;
			brdcmn.isLegend = column.name === legendKeyName ? 1 : 0;
			if (brdcmn.isLegend) brdcmn.isDisplayable = 0;

			return Board.app.models.BrdCmn.upsert(brdcmn, {transaction: tx});
		  }));
		}

		async function updateLanguage(board, languages){
		  const boardData = (await _executeFind(languages, {transaction: tx})).find(data => data.languageCode === board.lang);

		  if (board.code === board.lang) {
			boardData.name = board.name;
			boardData.description = board.description;
			boardData.templateid = board.templateid;
			boardData.translatedat = new Date();
			return boardData.save({transaction: tx});
		  }

		  if (boardData.name !== board.name && boardData.description !== board.description && boardData.templateid !== board.templateid) {
			return Board.app.models.BrdLge.upsert({
			  boardId: board.id,
			  languageCode: board.code,
			  defaultLge: board.lang,
			  name: board.name,
			  description: board.description,
			  templateid: board.templateid
			}, {transaction: tx});
		  }
		}

		await tx.commit();
		_flushCache(/user-\w+$/);
		return {message: 'Board -' + board.name + '- Successfully updated'}
	  } catch (e) {
		await tx.rollback();
		console.error(e);
		throw e;
	  }
	})()
		.then(result =>{
		  callback(null, result);
		})
		.catch(callback);
  };

  /**
   * @adminMaps
   * @desc
   * If the user is registered in RankCountry Returns fusion tables
   *
   * 0. This.listTable
   * 0.1. Get Refresh
   * 0.2. Ask for FusionTables
   * 1. Ask for localTables
   * 2. Minus & Return
   *
   * @param {string} email - tableId of a certain user already registered on RankCountry.
   * @param {object} cb - Callback.
   *
   */
  Board.remoteMethod('admin', {
	description: 'Get Admin Maps: FusionTableIds MINUS localTableIds',
	http: {path: '/admin', verb: 'GET'},
	returns: {arg: 'adminMaps', type: 'String'}
  });
  Board.admin = function(callback){
	(async () =>{
	  const ctx = LoopBackContext.getCurrentContext();
	  const currentUser = ctx && ctx.get('currentUser');

	  try {
		function mapper(table){
		  const map = Object.assign({}, table);
		  delete map.tableId;
		  return map;
		}

		let adminMaps = boardCache.get(`user-${currentUser.sub}`) || [];
		if (adminMaps.length) return adminMaps.map(mapper);

		async function filterMaps(maps, columnIds){
		  return (await Promise.all(maps.map(async fusionMap =>{
			const map = await Board.app.models.Map.find({where: {tableId: fusionMap.tableId}});

			if (!map.length) {
			  if (columnIds && fusionMap.columns) {
				fusionMap.columns.forEach(column =>{
				  column.id = column.columnId + 1;
				});
			  }
			  else {
				console.error(new Error(`** FusionTable without columns:\nid: ${fusionMap.tableId}\nname: ${fusionMap.name}`));
				return;
			  }

			  return fusionMap;
			}
		  }))).filter(map => map);
		}

		async function requestTables(user, pageToken){
		  let requestOptions = {
			fields: 'items(columns,description,name,tableId),nextPageToken',
			userId: 'me',
			maxResults: 500,
			auth: oauth2Client
		  };

		  if (pageToken) requestOptions.pageToken = pageToken;
		  oauth2Client.setCredentials(user);
		  const {data} = await fusiontables.table.list(requestOptions);
		  const filtered = await filterMaps(data.items, true);
		  adminMaps = adminMaps.concat(filtered);

		  if (!data.items.nextPageToken) {
			adminMaps.sort((a, b) => a.name.toUpperCase().localeCompare(b.name.toUpperCase()));

			for (let i = adminMaps.length; i > 0; i--) {
			  adminMaps[i - 1]['id'] = i;
			}

			return adminMaps;
		  }

		  return requestTables(user, data.items.nextPageToken);
		}

		adminMaps = await requestTables(currentUser);
		boardCache.set(`user-${currentUser.sub}`, adminMaps, 60 * 60 * 1000);
		return adminMaps.map(mapper);
	  } catch (e) {
		console.error(e);
		throw e;
	  }
	})()
		.then(result =>{
		  callback(null, result);
		})
		.catch(callback);
  };

  /**
   * @userBoard
   * @desc
   * If the user is registered in RankCountry Returns fusion table data
   *
   * 2. Minus & Return
   *
   * @param {string} adminMapId - for return
   * @param {object} cb - Callback.
   *
   */
  Board.remoteMethod('userBoard', {
	description: 'Get full FusionTable from user maps',
	accepts: {arg: "boardId", type: "number", required: true},
	http: {path: '/userBoard', verb: 'GET'},
	returns: {arg: 'userBoard', type: 'String'}
  });
  Board.userBoard = function(adminMapId, callback){
	(async () =>{
	  const ctx = LoopBackContext.getCurrentContext();
	  const currentUser = ctx && ctx.get('currentUser');

	  try {
		const table = boardCache.get(`user-${currentUser.sub}`).find(table => table.id === adminMapId);
		oauth2Client.setCredentials(currentUser);
		const results = !table.styles || !table.templates ? await _stylesTemplatesRequest(table.tableId, oauth2Client) : {};
		Object.assign(results, table);
		delete results['tableId'];
		results.userBoard = true;

		return results;
	  } catch (e) {
		console.error(e);
		throw e;
	  }
	})()
		.then(result =>{
		  callback(null, result);
		})
		.catch(callback);
  };

  /**
   * @getBasicBoard
   * @desc
   * Get Basic Board: Retrieves a specific (mini) information of board given its ID ;)
   *
   * @param {string} boardId - boardId.
   * @param {object} cb - Callback
   *
   */
  Board.remoteMethod('getBasicBoard', {
	description: 'Get Basic Board: Retrieves a specific (mini) information of board given its ID ;)',
	accepts: {arg: "boardId", type: "Number", required: true},
	http: {path: '/:boardId/basic', verb: 'GET'},
	returns: {arg: 'getBasicBoard', type: 'Object'}
  });
  Board.getBasicBoard = function(boardId, callback){
	(async () =>{
	  const ctx = LoopBackContext.getCurrentContext();
	  const credentials = ctx && ctx.get('credentials');
	  const tableId = ctx && ctx.get('tableId');
	  const lang = ctx && ctx.get('lang');

	  let basicBoard = boardCache.get(`basic-board-${boardId}-${lang}`) || {};
	  if (basicBoard && basicBoard.id) return basicBoard;

	  const tx = await Board.beginTransaction({isolationLevel: Board.app.models.Map.Transaction.READ_COMMITTED});

	  try {
		const board = await Board.findById(boardId);
		const boardData = (await _executeQuery(`SELECT * FROM boards WHERE id=? AND code=?`, [board.id, lang]))[0];
		Object.assign(basicBoard, boardData, board.__data);
		basicBoard.categories = (await _executeFind(board.brdcries)).map(category => category.categoryId);

		const brdcmns = await _executeQuery('SELECT * FROM BrdCmn BC JOIN `Column` C ON C.id = BC.Column_id WHERE Board_id = ? AND C.source IS NULL', board.id);
		basicBoard.brdcmns = brdcmns.filter(column =>{
		  column.id = column.columnId + 1;

		  if (column.isLocation) {
			basicBoard.location = column.id;
		  } else if (column.isMeasurement) {
			basicBoard.measurement = {
			  isDisplayable: column.isDisplayable,
			  id: column.id,
			  customName: column.customName
			};
		  } else return true;
		}).map(col => col.id);

		oauth2Client.setCredentials(credentials);
		Object.assign(basicBoard, await _stylesTemplatesRequest(tableId, oauth2Client));
		basicBoard.columns = await _columns(oauth2Client);

		async function _columns(oauth2Client){
		  oauth2Client.setCredentials(credentials);
		  const {data} = await fusiontables.table.get({tableId: tableId, userId: 'me', auth: oauth2Client});
		  const map = await _executeFind(board.map);
		  const columns = await _executeFind(map.columns);
		  const regExp = new RegExp("from '([\\w-]+)' to '([\\w-]+)' \\((.+)\\)");

		  await Promise.all(data.columns.map(async fusionColumn =>{
			const currentColumn = columns.find(col => fusionColumn.columnId === col.columnId) || {};
			fusionColumn['mapId'] = map['id'];

			if (!currentColumn.id || fusionColumn.name !== currentColumn.name || fusionColumn.type !== currentColumn.type || fusionColumn.formatPattern !== currentColumn.formatPattern) {
			  const langInfo = fusionColumn.description && fusionColumn.description.match(regExp);
			  if (langInfo) {
				const source = columns.find(col => col.name === langInfo[3]);
				fusionColumn.languageCode = langInfo[2];
				fusionColumn.source = source.id;
			  }

			  const column = Object.assign(currentColumn, fusionColumn);
			  try {
				return await Board.app.models.Column.upsert(column, {transaction: tx});
			  } catch (e) {
				console.error(e);
				console.error(map.tableId, column, boardData);
				return column;
			  }
			}
		  }));

		  return data.columns.filter(col => col.formatPattern !== 'HIGHLIGHT_UNTYPED_CELLS').map(column => Object.assign({
			columnId: column.columnId,
			name: column.name,
			type: column.type,
			id: column.columnId + 1
		  }));
		}

		basicBoard.templateid = basicBoard.templateId;
		delete basicBoard.mapId;
		delete basicBoard.templateId;

		boardCache.set(`basic-board-${boardId}-${lang}`, basicBoard, 10 * 60 * 1000);
		await tx.commit();
		return basicBoard;
	  } catch (e) {
		await tx.rollback();
		console.error(e);
		throw e;
	  }
	})()
		.then(result =>{
		  callback(null, result);
		})
		.catch(callback);
  };

  /**
   * @getFullBoard
   * @desc
   * Get Full Board: Retrieves a specific board given its ID ;)
   *
   * @param {string} boardId - boardId.
   * @param {object} cb - Callback
   *
   */
  Board.remoteMethod('getFullBoard', {
	description: 'Get Full Board: Retrieves a specific board given its ID ;)',
	accepts: {arg: "boardId", type: "String", required: true},
	http: {path: '/:boardId/full', verb: 'get'},
	returns: {arg: 'getFullBoard', type: 'Object'}
  });
  Board.getFullBoard = function(boardId, callback){
	(async () =>{
	  const ctx = LoopBackContext.getCurrentContext();
	  const credentials = ctx && ctx.get('credentials');
	  const lang = ctx && ctx.get('lang');
	  const tableId = ctx && ctx.get('tableId');

	  try {
		const fullBoard = boardCache.get(`board-${boardId}-${lang}`) || {};
		if (fullBoard && fullBoard.id) return fullBoard;

		const board = await Board.findById(boardId);
		const brdLge = (await _executeQuery('SELECT * FROM boards WHERE id=? AND code=?', [boardId, lang]))[0];
		const brdcmns = await Board.app.models.BrdCmn.getBoardColumns(board.id, lang);

		const order = {direction: board.direction, columns: []};
		const layer = {
		  heatmap: {
			enabled: false
		  },
		  options: {
			styleId: board.styleid,
			templateId: brdLge.templateId
		  }
		};

		let legend;
		let legendKey = {};
		const columns = brdcmns.filter(brdcmn =>{
		  if (brdcmn.isMeasurement) {
			order.columns.push(brdcmn);
			legend = brdcmn;
			legend.data = [];
		  }

		  if (brdcmn.isLegend) {
			legendKey = brdcmn;
			legendKey.data = [];
		  }
		  if (brdcmn.isLocation) {
			layer.query = {
			  select: 'col' + brdcmn.columnId,
			  from: tableId,
			  where: ''
			};
			return false;
		  }
		  return true;
		}).map(brdcmn => Object.assign({
		  name: brdcmn.name,
		  columnId: brdcmn.columnId,
		  displayable: brdcmn.isDisplayable,
		  customName: brdcmn.customName === '' ? undefined : brdcmn.customName,
		  type: brdcmn.type,
		  isLegend: brdcmn.isLegend,
		  formatPattern: brdcmn.formatPattern
		}));

		columns.unshift({name: 'ISO', type: 'String', displayable: false});
		oauth2Client.setCredentials(credentials);
		const {data} = await fusiontables.query.sqlGet({
		  fields: 'rows',
		  sql: _querySerializer(tableId, columns, order),
		  userId: 'me',
		  auth: oauth2Client
		});

		let rows = data.rows.map(field =>{
		  const row = {};
		  columns.forEach((column, i) =>{
			row[column.name] = column.type.toLocaleLowerCase() === 'number' ? Number(field[i]) || 0 : field[i];

			column.name === legend.name && legend.data.push(row[column.name]);
			column.name === legendKey.name && legendKey.data.push(row[column.name]);

			if (column.formatPattern && column.formatPattern !== 'NONE') {
			  const format = column.formatPattern.toLocaleUpperCase();
			  const pattern = formatPatters.DATE[format] || formatPatters.NUMERIC[format];
			  const formatter = formatPatters.DATE[format] ? moment : numeral;
			  row[column.name] = formatter(row[column.name]).format(pattern);
			}
		  });
		  return row;
		});

		const legends = await _legendByStyle(tableId, board.styleid, legend.data, legendKey.data, oauth2Client);
		legends.filter.customName = legend.customName === '' ? undefined : legend.customName;
		legends.filter.id = legend.columnId;

		const {data: attributionData} = await fusiontables.table.get({
		  fields: 'attribution, attributionLink',
		  userId: 'me',
		  tableId: tableId,
		  auth: oauth2Client
		});

		Object.assign(fullBoard, {
		  id: board.id,
		  board: rows,
		  columns: columns,
		  layer: btoa(JSON.stringify(layer)),
		  options: {lat: 0, lon: 0, zoom: 3},
		  legend: legends,
		  attribution: attributionData
		});
		boardCache.set(`board-${boardId}-${lang}`, fullBoard, 60 * 60 * 1000);
		return fullBoard;
	  } catch (e) {
		console.error(e);
		throw e;
	  }
	})()
		.then(result =>{
		  callback(null, result);
		})
		.catch(callback);
  };

  /**
   * @listNames
   * @desc
   * Returns all the boards only with their names and id
   *
   * @param {object} cb - Callback
   *
   */
  Board.remoteMethod('listNames', {
	description: 'Returns all the boards only with their names and id',
	http: {path: '/listNames', verb: 'GET'},
	returns: {arg: 'listNames', type: 'Object'}
  });
  Board.listNames = function(callback){
	(async () =>{
	  const ctx = LoopBackContext.getCurrentContext();
	  const lang = ctx && ctx.get('lang');
	  let list = boardCache.get(`boards-names-${lang}`);
	  if (list) return list;

	  const query = [];
	  query.push('SELECT id, name, description');
	  query.push('FROM boards');
	  query.push('WHERE code = ?');

	  const boards = await _executeQuery(query.join(' '), lang);

	  query.length = 0;
	  query.push('SELECT lang, name');
	  query.push('FROM boards');
	  query.push('WHERE code = lang');
	  query.push('AND id = ?');
	  query.push('AND code NOT LIKE ?');
	  list = {};
	  await Promise.all(boards.map(async board =>{
		board.alternates = (await _executeQuery(query.join(' '), [board.id, lang])).map(alternate =>{
		  alternate.uid = alternate.name.split(' ').join('-').toLowerCase();
		  return alternate;
		});

		list[board.id] = {
		  uid: board.name.split(' ').join('-').toLowerCase(),
		  name: board.name,
		  description: board.description,
		  alternates: board.alternates
		};
	  }));

	  boardCache.set(`boards-names-${lang}`, list, 8 * 60 * 60 * 1000);
	  return list;
	})().then(result => callback(null, result))
		.catch(err =>{
		  console.error(err);
		  callback(err);
		});
  };

  /**
   * @buckets
   * @desc
   * Returns all the boards data filtered using the buckets requested.
   *
   * @param {Array} buckets.
   * @param {object} cb - Callback
   *
   */
  Board.remoteMethod('buckets', {
	description: 'Return the processed bucket required',
	accepts: [
	  {
		arg: 'bucket[]',
		type: 'array',
		required: true,
		http: {source: 'body'}
	  },
	  {arg: "include_all", type: "Boolean"}
	],
	http: {path: '/buckets', verb: 'post'},
	returns: {arg: 'buckets', type: 'Object'}
  });
  Board.buckets = function(buckets, include_all, callback){
	(async () =>{
	  const ctx = LoopBackContext.getCurrentContext();
	  const lang = ctx && ctx.get('lang');

	  try {
		const ISOS = [];
		const countries = [];
		const length = buckets.length;

		await Promise.all(buckets.map(async bucket =>{
		  const board = (await _executeQuery('SELECT * FROM boards WHERE id=? AND code=?', [bucket.id, lang]))[0];
		  const {credentials, tableId} = await Board.app._boardCredentials(board.id);
		  const column = (await Board.app.models.BrdCmn.getBoardColumns(board.id, lang)).find(brdcmn => brdcmn.columnId === bucket.column);
		  const columns = [{name: 'ISO', type: 'String', displayable: false}, column];
		  const order = {columns: [column], direction: 'ASC'};

		  oauth2Client.setCredentials(credentials);
		  const {data} = await fusiontables.query.sqlGet({
			fields: 'rows',
			sql: _querySerializer(tableId, columns, order),
			userId: 'me',
			auth: oauth2Client
		  });

		  bucket.countries = data.rows.map(function(field){
			const row = {};

			columns.map(function(column, i){
			  row[column.name] = column.type.toLocaleLowerCase() === 'number' ? Number(field[i]) || 0 : field[i];

			  if (column.formatPattern && column.formatPattern !== 'NONE') {
				const format = column.formatPattern.toLocaleUpperCase();
				const pattern = formatPatters.DATE[format] || formatPatters.NUMERIC[format];
				const formatter = formatPatters.DATE[format] ? moment : numeral;
				row[column.name] = formatter(row[column.name]).format(pattern);
			  }
			});

			return row;
		  }).filter(row =>{
			if (row.ISO === '') return false;
			row.interval = bucket.filter.findIndex(interval =>{
			  return row[column.name] >= interval.min &&
				  (interval.last ? row[column.name] <= interval.max : row[column.name] < interval.max);
			});

			if (include_all && row.interval === -1) return;

			let i = ISOS.indexOf(row.ISO);
			const data = JSON.parse(JSON.stringify(row));
			data.id = board.id;
			delete data.ISO;

			if (i === -1) {
			  i = ISOS.push(row.ISO) - 1;
			  countries[i] = {
				ISO: row.ISO,
				data: [],
				include: function(){
				  return include_all ? this.data.length === length : this.data.find(board => board.interval !== -1);
				}
			  };
			}

			countries[i].data.push(data);

			return row.interval !== -1;
		  });

		  bucket.board = board.name;
		  return bucket;
		}));

		return {buckets: buckets, countries: countries.filter(country => country.include())};
	  } catch (e) {
		console.error(e);
		throw e;
	  }
	})().then(result => callback(null, result))
		.catch(callback);
  };

  /**
   * @genSitemap
   * @desc
   * Returns the string of the sitemap.xml for the available maps in RankCountry
   *
   * @param {String} cb - Callback
   *
   */
  Board.remoteMethod('genSitemap', {
	description: 'Returns the string of the sitemap.xml for the available maps in RankCountry',
	accepts: [
	  {arg: "lang", type: "String"},
	  {arg: "category", type: "Number"},
	  {arg: "size", type: "Number"},
	  {arg: "from", type: "Number"},
	],
	http: {path: '/sitemap', verb: 'GET'},
	returns: {arg: 'sitemap', root: true, type: 'Object'}
  });
  Board.genSitemap = function(lang, category, size, from, callback){
	(async () =>{
	  let result = {
		_attrs: {
		  xmlns: 'http://www.sitemaps.org/schemas/sitemap/0.9',
		  version: '2.0'
		}
	  };
	  const list = [];
	  const basePath = 'https://rankcountry.com';

	  if (!lang) {
		for (const language of await Board.app.models.Language.find()) {
		  list.push({
			_name: 'sitemap',
			_content: {
			  loc: `${basePath}/${language.code}/sitemap.xml`,
			  name: language.name
			}
		  });
		}

		result._name = 'sitemapindex';
	  } else {
		const query = [];

		if (category) {
		  query.push('SELECT B.code, B.name');
		  query.push('FROM categories C');
		  query.push('JOIN boards B ON C.board = B.id AND C.code = B.code');
		  query.push('WHERE C.code = ? AND C.category = ?');
		  query.push('ORDER BY B.id');
		} else {
		  query.push('SELECT code, name');
		  query.push('FROM boards');
		  query.push('WHERE code = lang AND code = ?');
		  query.push('ORDER BY code');
		}

		if (from) {
		  const table = query.join(' ');
		  query.length = 0;

		  query.push('SELECT * FROM (');
		  query.push(`SELECT q.*, @curRow := @curRow + 1 AS n FROM (${table}) q`);
		  query.push(`JOIN (SELECT @curRow := 0) r`);
		  query.push(`) t`);

		  from && query.push(`WHERE n > ${from}`);
		}

		size && query.push(`LIMIT ${size}`);
		const path = basePath + (lang !== 'en' ? '/' + lang : '');
		list.push({
		  _name: 'url',
		  _content: {
			loc: path,
			// lastmod: moment().format('YYYY-10-01'),
			// changefreq: 'daily',
			// priority: 1
		  }
		});

		for (const board of await _executeQuery(query.join(' '), [lang, category])) {
		  list.push({
			_name: 'url',
			_content: {
			  loc: `${path}/${board.name.split(' ').join('-').toLowerCase()}`,
			  // lastmod: moment().format('YYYY-10-01'),
			  // changefreq: 'daily',
			  // priority: 1
			}
		  });
		}

		result._name = 'urlset';
	  }

	  result._content = list;
	  return result;
	})()
		.then(result => callback(null, result))
		.catch(error => callback(error));
  };

  /**
   * @translateFullBoard
   * @desc
   *
   * @param {string} boardId - Board ID.
   *
   */
  Board.remoteMethod('translateFullBoard', {
	description: 'Translate Full board',
	accepts: [
	  {arg: "boardId", type: "String", required: true},
	  {arg: "purge", type: "Boolean"}
	],
	http: {path: '/:boardId/translate', verb: 'get'},
	returns: {arg: 'translatedFullBoard', type: 'Object'}
  });
  Board.translateFullBoard = (boardId, purge, callback) =>{
	const ctx = LoopBackContext.getCurrentContext();
	const requestLang = ctx && ctx.get('lang');
	const tableId = ctx && ctx.get('tableId');
	const credentials = ctx && ctx.get('credentials');
	const formatTranslatedColumns = 'HIGHLIGHT_UNTYPED_CELLS';
	let query = [];
	query.push('SELECT');
	query.push('idFrom, idTo, columnIdFrom, columnIdTo, nameFrom, nameTo, langFrom, langTo, mapId, translated, isDefault, type, customNameFrom');
	query.push('FROM i18nBoardColumnsView');
	query.push('WHERE boardId = ?');
	if (!purge) {
	  query.push('AND (translated = 0 OR isDefault = 1)'); //ONLY TRANSLATE/UPDATE DATA OF NEW COLUMNS
	}

	(async () =>{
	  try {
		// Translate Board Name to all Languages
		await Board.translateBoard(boardId);
		console.log('Board name Translated');

		let results = await _executeQuery(query.join(' '), boardId);
		if (results.length > 0) {
		  let languages = {};
		  let stringColumns = {};
		  let newColumns = {};
		  let newLanguages = [];
		  let brdCmns = {};
		  let langFrom = requestLang;
		  let mapId = 0;
		  let insertColumnsResults = {};

		  // Mapping columns names from DB to translate
		  results.forEach(result =>{
			let columnFrom = {
			  id: result.idFrom,
			  columnId: result.columnIdFrom,
			  lang: result.langFrom,
			  name: result.nameFrom,
			  customName: result.customNameFrom
			};

			let columnTo = {
			  id: result.idTo,
			  columnId: result.columnIdTo,
			  lang: result.langTo,
			  name: result.nameTo
			};

			if (result.isDefault) {
			  langFrom = result.langFrom;
			  mapId = result.mapId;
			  if (result.customNameFrom && result.customNameFrom.length > 0) {
				brdCmns[result.customNameFrom] = columnFrom;
			  }
			} else {
			  if (result.type === 'STRING') {
				stringColumns[columnFrom.name] = columnFrom;
			  }
			  languages[result.langTo] = result.langTo;
			  if (!result.translated) {
				newColumns[columnFrom.name] = columnFrom;
				newLanguages[result.langTo] = result.langTo;
			  } else {
				insertColumnsResults[columnTo.lang] = insertColumnsResults[columnTo.lang] || {};
				insertColumnsResults[columnTo.lang][columnFrom.columnId] = {
				  column: columnTo,
				  fromColumn: columnFrom,
				  lang: columnTo.lang,
				  brdcmn: {
					boardId: boardId,
					columnId: columnTo.id,
					customName: columnFrom.customName
				  }
				}
			  }
			}
		  });
		  stringColumns = Object.values(stringColumns);

		  //If exists new columns insert them into Fusion Tables and Database
		  if (Object.keys(newColumns).length > 0) {
			console.log('New Columns Detected', Object.keys(newColumns));
			// Translate new Column Names to all languages
			let columnNamesTranslation = await Board.app.models.Language.translate(
				{lang: langFrom, queries: Object.keys(newColumns)},
				Object.keys(newLanguages)
			);
			newColumns = Object.values(newColumns);
			for (let i = 0; i < columnNamesTranslation.length; i++) {
			  let columnNameTranslated = columnNamesTranslation[i];
			  for (let j = 0; j < columnNameTranslated.queries.length; j++) {
				let nameTranslated = columnNamesTranslation[i].queries[j];
				let newColumn = newColumns[j];
				console.log('Column Translated', columnNameTranslated.from, columnNameTranslated.lang, newColumn.name, nameTranslated);
				try {
				  // Insert translated columns name into Fusion Tables
				  oauth2Client.setCredentials(credentials);
				  let insertResult = await fusiontables.column.insert({
					tableId: tableId,
					userId: 'me',
					auth: oauth2Client,
					requestBody: {
					  name: nameTranslated,
					  type: 'STRING',
					  formatPattern: formatTranslatedColumns,
					  description: `Translated by RankCountry from '${columnNameTranslated.from}' to '${columnNameTranslated.lang}' (${newColumn.name})`
					}
				  });
				  insertResult.data['mapId'] = mapId;
				  insertResult.data['languageCode'] = columnNameTranslated.lang;
				  insertResult.data['source'] = newColumn.id;
				  console.log('Column Created into FusionTable', newColumn.name, '/', insertResult.data.name);
				  // Insert translated columns name into Database
				  let dbInsertResult = await Board.app.models.Column.create(insertResult.data);
				  console.log('Column Created into Database', insertResult.data.name);
				  insertColumnsResults[columnNameTranslated.lang] = insertColumnsResults[columnNameTranslated.lang] || {};
				  insertColumnsResults[columnNameTranslated.lang][newColumn.columnId] = {
					column: insertResult.data,
					fromColumn: newColumn,
					lang: columnNameTranslated.lang,
					brdcmn: {
					  boardId: boardId,
					  columnId: dbInsertResult.id,
					  customName: newColumn.customName
					}
				  };
				} catch (e) {
				  console.error(e);
				}
			  }
			}
		  }

		  // Upsert BrdCmn and translate Custom Names
		  if (Object.keys(brdCmns).length > 0 && Object.keys(languages).length > 0) {

			let customNamesTranslation = await Board.app.models.Language.translate(
				{lang: langFrom, queries: Object.keys(brdCmns)},
				Object.keys(languages)
			);

			for (let i = 0; i < customNamesTranslation.length; i++) {
			  let customNameTranslated = customNamesTranslation[i];
			  for (let j = 0; j < customNameTranslated.queries.length; j++) {
				try {
				  let brdCmnTranslated = Object.values(brdCmns)[j];
				  let brdCmn = insertColumnsResults[customNameTranslated.lang][brdCmnTranslated.columnId].brdcmn;
				  brdCmn.customName = customNameTranslated.queries[j];
				  await Board.app.models.BrdCmn.upsert(brdCmn);
				  console.log('BrdCmn upserted sucessfully', brdCmn);
				} catch (e) {
				  console.error('Error upserting BrdCmn', e);
				}
			  }
			}
		  }

		  await _generateTemplates(boardId, tableId, credentials);

		  //If exists string columns then translate data
		  if (stringColumns.length > 0) {
			console.log('String Columns Detected', stringColumns);
			// Translate Data from Fusion Table and Update translated columns
			let dataDictionary = {};
			// Getting data from Fusion Table grouped by values to reduce costs with the Translate API
			oauth2Client.setCredentials(credentials);
			let queryResult = await fusiontables.query.sqlGet({
			  fields: 'rows',
			  sql: _querySerializer(tableId, stringColumns, null, stringColumns),
			  userId: 'me',
			  auth: oauth2Client
			});

			queryResult.data.rows.forEach(result =>{
			  stringColumns.forEach((column, index) =>{
				dataDictionary[result[index]] = {column: column, value: result[index]};
			  });
			});

			// Translating the dictionary
			let rowsTranslated = await Board.app.models.Language.translate({
			  lang: langFrom,
			  queries: Object.keys(dataDictionary)
			}, Object.keys(languages));

			// Updating translated data to Fusion Table
			for (let i = 0; i < rowsTranslated.length; i++) {
			  let rowTranslated = rowsTranslated[i];
			  for (let j = 0; j < rowTranslated.queries.length; j++) {
				try {
				  let dataTranslated = rowTranslated.queries[j];
				  let dictionaryArray = Object.values(dataDictionary)[j];
				  let newColumn = insertColumnsResults[rowTranslated.lang][dictionaryArray.column.columnId];
				  let wheres = {};
				  wheres[dictionaryArray.column.name] = dictionaryArray.value;
				  let sets = {};
				  sets[newColumn.column.name] = dataTranslated;
				  oauth2Client.setCredentials(credentials);
				  await fusiontables.query.sql({
					sql: _queryUpdateSerializer(tableId, sets, wheres),
					userId: 'me',
					auth: oauth2Client
				  });
				  console.log('Data UPDATED successfully in Fusion Table', sets, wheres);
				  await _wait(2000); // To avoid quota of 30 write requests per minute per table
				} catch (e) {
				  console.error('Error Updated translated data into Fusion Table', e);
				}
			  }
			}
		  }

		  return callback(null, 'Board Translated Successfully');
		} else {
		  return callback(null, 'This board doesn\'t have columns to translate');
		}
	  } catch (e) {
		console.error('Translation Error', e);
		return callback(e);
	  }
	})();
  };

  /**
   * @getDescription
   * @desc
   *
   * @param {number} boardId - the id of the board to be translated.
   *
   */
  Board.translateBoard = async function(boardId){
	const origins = {};
	const boardList = {};
	const properties = ['name', 'description'];
	const query = [];
	query.push(`SELECT id, code 'to', lang 'from', name, description, templateId`);
	query.push('FROM boards');
	query.push('WHERE code NOT LIKE lang');
	query.push(boardId ? 'AND id = ?' : 'LIMIT 50');
	const results = await _executeQuery(query.join(' '), boardId);
	results.forEach(result =>{
	  boardList[result.id] = boardList[result.id] || {};
	  boardList[result.id][result.to] = _mapProperties(result, properties);
	  boardList[result.id][result.to].defaultLge = result.from;
	  boardList[result.id][result.to].templateid = result.templateId;

	  let target = origins[result.from] || {};
	  let boardIds = target[result.to] || [];
	  boardIds.push(result.id);
	  target[result.to] = boardIds;
	  origins[result.from] = target;
	});

	let translations = [];
	Object.keys(origins).forEach(from =>{
	  Object.keys(origins[from]).forEach(to =>{
		let queries = [];
		let i = 0;
		origins[from][to].forEach((category) =>{
		  properties.forEach(prop =>{
			queries.push(boardList[category][to][prop]);
			boardList[category][to][prop] = i++;
		  });
		});

		translations.push([{lang: from, queries: queries}, to]);
	  });
	});

	const upserts = [];
	const boards = Object.keys(boardList);
	let translation;

	for (const args of translations) {
	  translation = await Board.app.models.Language.translate(args[0], args[1]);
	  translation = translation[0];

	  boards.forEach(id =>{
		let board = boardList[id][translation.lang];
		if (board) {
		  board.boardId = id;
		  board.languageCode = translation.lang;
		  properties.forEach(prop =>{
			board[prop] = translation.queries[board[prop]];
		  });

		  upserts.push(Board.app.models.BrdLge.upsert(board));
		}
	  });
	}

	return await Promise.all(upserts);
  };

  /***
   * Generate a Template for each Language
   * @param boardId
   * @param tableId
   * @param credentials
   * @returns {Promise<void>}
   * @private
   */
  async function _generateTemplates(boardId, tableId, credentials){
	try {
	  let languages = await _executeQuery('SELECT code FROM Language;');
	  languages = languages.map(lang => lang.code);
	  oauth2Client.setCredentials(credentials);
	  let templates = await fusiontables.template.list({
		tableId: tableId,
		userId: 'me',
		auth: oauth2Client
	  });
	  for (let i = 0; i < languages.length; i++) {
		let body = [];
		let lang = languages[i];
		let brdcmns = await Board.app.models.BrdCmn.getBoardColumns(boardId, lang);
		for (let j = 0; j < brdcmns.length; j++) {
		  let column = brdcmns[j];
		  if (column && (column.isMeasurement || column.isDisplayable)) {
			body.push(`<b>${column.name}:</b> {col${column.columnId}}`);
			body.push('<br>');
		  }
		}
		let nameTemplate = `i18n Template (${lang})`;
		let BrdLge = await Board.app.models.BrdLge.findOne({where: {boardId: boardId, languageCode: lang}});
		oauth2Client.setCredentials(credentials);
		let template = templates.data.items.find(t => t.name === nameTemplate);
		if (template) {
		  oauth2Client.setCredentials(credentials);
		  await fusiontables.template.update({
			tableId: tableId,
			templateId: template.templateId,
			userId: 'me',
			auth: oauth2Client,
			requestBody: {
			  name: nameTemplate,
			  body: body.join(''),
			}
		  });
		  console.log(`Template Updated on Fusion Table`, template.name, template.templateId);
		} else {
		  oauth2Client.setCredentials(credentials);
		  template = await fusiontables.template.insert({
			tableId: tableId,
			userId: 'me',
			auth: oauth2Client,
			requestBody: {
			  name: nameTemplate,
			  body: body.join(''),
			}
		  });
		  console.log(`Template Created on Fusion Table`, template.data.name, template.data.templateId);
		}
		BrdLge.templateid = template.data ? template.data.templateId : template.templateId;
		await BrdLge.save();
		console.log(`Template Updated into Database`, BrdLge.name, BrdLge.templateid);

	  }
	} catch (e) {
	  console.error(e);
	}
  }

  /***
   * Serialize UPDATE queries
   * @param tableId
   * @param setObject
   * @param whereObject
   * @returns {string}
   * @private
   */
  _queryUpdateSerializer = (tableId, setObject, whereObject) =>{
	let query = ['UPDATE'];
	query.push(tableId);
	query.push('SET');

	let formatter = (setObjectParam) =>{
	  let result = [];
	  Object.keys(setObjectParam).forEach((key, index) =>{
		if (index > 0) {
		  result.push(',');
		}
		result.push(`'${key.replace(/'/g, `\\'`)}'`);
		result.push('=');
		result.push(`'${setObjectParam[key].replace(/'/g, `\\'`)}'`);
	  });
	  return result.join(' ');
	};

	query.push(formatter(setObject));
	query.push('WHERE');
	query.push(formatter(whereObject));
	return query.join(' ');
  };

  /***
   * Execute a Query using Promises
   * @param query
   * @param queryParams
   * @param options
   * @returns {Promise<any>}
   * @private
   */
  function _executeQuery(query, queryParams, options){
	return new Promise((resolve, reject) =>{
	  queryParams = queryParams && !Array.isArray(queryParams) ? [queryParams] : queryParams;
	  Board.app.datasources['RCdatabase'].connector.execute(query, queryParams, options, (queryError, results) =>{
		if (queryError) return reject(queryError);
		resolve(results);
	  });
	});
  }

  /***
   * Execute a value resolution using Promises
   * @param func
   * @param params
   * @returns {Promise<any>}
   * @private
   */
  function _executeFind(func, params){
	if (typeof func !== 'function') throw new Error('the first params must be a function');
	return new Promise((resolve, reject) =>{
	  func(params, (error, results) =>{
		if (error) return reject(error);
		resolve(results);
	  });
	});
  }

  /***
   * Flush the cache for all the app.
   * @returns {Promise<any>}
   * @private
   */
  Board.remoteMethod('_flushCache', {
	description: 'Translate Full board',
	accepts: [
	  {arg: "exclude", type: "any"}
	],
	http: {path: '/flushCache', verb: 'get'},
	returns: {arg: 'cache', root: true, type: 'Object'}
  });
  Board._flushCache = _flushCache;

  async function _flushCache(exclude){
	let keys = boardCache.keys();
	if (Array.isArray(exclude)) keys = keys.filter(key => exclude.indexOf(key) === -1);
	if (exclude instanceof RegExp) keys = keys.filter(key => !exclude.test(key));

	return await Promise.all(keys.map(async key => Object.assign({name: key, flushed: boardCache.del(key)})));
  }

  /***
   * Wait Promise
   * @param timeout (miliseconds)
   * @returns {Promise<any>}
   * @private
   */
  _wait = (timeout) =>{
	return new Promise(resolve => setTimeout(resolve, timeout));
  };

  /***
   *
   * @param obj
   * @param properties
   * @private
   */
  function _mapProperties(obj, properties){
	let newObj = {};
	properties.forEach(prop =>{
	  newObj[prop] = obj[prop];
	});

	return newObj;
  }
};