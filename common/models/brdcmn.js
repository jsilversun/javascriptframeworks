module.exports = function(BrdCmn){

  // SQL Queries
  /**
   * @getBoardColumns
   * @desc
   * Returns all the columns of a given board.
   *
   * @param {string} boardId.
   * @param {string} lang.
   * @private
   */
  BrdCmn.getBoardColumns = async function(boardId, lang = 'en'){
	try {
	  const query = [`SELECT`];
	  query.push(`CASE WHEN C.type = "STRING" THEN IFNULL(V.columnIdTo, C.columnId) ELSE C.columnId END columnId,`);
	  query.push(`C.type,`);
	  query.push(`IFNULL(V.nameTo, C.name) name,`);
	  query.push(`C.formatPattern,`);
	  query.push(`IFNULL(V.customNameTo, C.customName) customName,`);
	  query.push(`C.isDisplayable,`);
	  query.push(`C.isLocation,`);
	  query.push(`C.isMeasurement,`);
	  query.push(`C.isLegend`);
	  query.push(`FROM (SELECT`);
	  query.push(['C.columnId columnId','C.id', 'C.type', 'C.name', 'C.formatPattern', 'BC.*']);
	  query.push('FROM `Column` C');
	  query.push('JOIN `BrdCmn` BC ON (C.id = BC.Column_id) WHERE C.source IS NULL) C');
	  query.push(`LEFT JOIN i18nBoardColumnsView V ON V.idFrom = C.id AND V.langTo = ?`);
	  query.push(`WHERE C.Board_id = ?`);

	  return _executeQuery(query.join(' '), [lang, boardId]);
	} catch (e) {
	  console.error(e);
	  throw e;
	}
  };

  /***
   * Execute a Query using Promises
   * @param query
   * @param queryParams
   * @param options
   * @returns {Promise<any>}
   * @private
   */
  function _executeQuery(query, queryParams, options){
	return new Promise((resolve, reject) =>{
	  queryParams = queryParams && !Array.isArray(queryParams) ? [queryParams] : queryParams;
	  BrdCmn.app.datasources['RCdatabase'].connector.execute(query, queryParams, options, (queryError, results) =>{
		if (queryError) return reject(queryError);
		resolve(results);
	  });
	});
  }
};
