<!-- Rank Country Issue Template -->

<!-- Please provide as much detail as possible for us to fix your issue -->
<!-- Need help with Markdown? Check this out https://gitlab.com/help/user/markdown.md -->
<!-- Remove comments and any heading sections you did not fill out -->

#### Issue Description
<!-- Describe below when the issue happens and how to reproduce it -->

#### Expected Behaviour / Suggested Solution
<!-- Describe below when the issue happens and how to reproduce it -->
 
#### Screenshot
<!-- Add a screenshot of your issue -->

#### Browser

- ✅ All of them
- ? Chrome
- ? Firefox
- ? Opera
- ? Safari
- ? Edge

#### OS

- ✅ All of them
- ? Windows
- ? macOS
- ? Linux
- ? Android
- ? iOS

#### Your Code
<!-- If relevant, paste all of your challenge code in here -->
```js



```