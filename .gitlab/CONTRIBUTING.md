# Contributing to Rank Country

Rank Country App first version is currently under development. 

## Bugs

Development uses Gitlab Issues. Updating the board so the progress it's clear to the team.

* [Guidelines](https://chris.beams.io/posts/git-commit/)

## How to Get in Touch

* [Gabriel Luis Whatsapp](callto:‭+19492286655‬)
* [Roy Calderon Telegram](http://telegram.me/RoyCalderon)

## Style Guide

### Code

#### JavaScript

* Use semicolons;
* Prefer `'` over `"`
* 80 character line length

#### GIT Commits

```git
# <type>: <subject>
# |<----  Using a Maximum Of 50 Characters  ---->|

# Explain why this change is being made
# |<----   Try To Limit Each Line to a Maximum Of 72 Characters   ---->|

# Provide links or keys to any relevant tickets, articles or other resources
# Example: Github issue #23
```

##### Type can be 
    feat     (new feature)
    fix      (bug fix)
    refactor (refactoring production code)
    style    (formatting, missing semi colons, etc; no code change)
    docs     (changes to documentation)
    test     (adding or refactoring tests; no production code change)
    chore    (updating grunt tasks etc; no production code change)
##### Remember to
    Capitalize the subject line
    Use the imperative mood in the subject line
    Do not end the subject line with a period
    Separate subject from body with a blank line
    Use the body to explain what and why vs. how
    Can use multiple lines with "-" for bullet points in body
